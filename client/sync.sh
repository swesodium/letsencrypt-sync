#/bin/bash

# Settings (Required to be updated).
remote_ip="0.0.0.0"                        # Remote host where Let's Encrypt is installed & where certs are signed. 

# Defaults (Can be left unchanched).
remote_user="root"                         # Remote host user (SSH)
remote_folder="/etc/letsencrypt/live"      # Remote host folder (default).
local_folder="/etc/letsencrypt/proxied"    # Local folder.
post_action="nginx"

mkdir -p $local_folder

# ---- Sync Certs ----
rsync -avh $remote_user@$remote_ip:$remote_folder/* $local_folder --copy-links --delete
echo "Sync complete"
chown -R root:root $local_folder

# ---- Post Hook Actions ----
if [ $post_action == "nginx" ]
then
  echo "Reloading NGINX..."

  nginx -t 2>/dev/null > /dev/null
  if [[ $? == 0 ]]; then
   echo "success"
   systemctl reload nginx
  fi

fi
